#include "cabecera.h"
#include <unistd.h>  
#include <stdio.h>
#define VALOR 100

/*opcion (
    s -> h m s
    d -> a m d
    )
*/
int main(int argc, char *argv[])  
{
    int opt;
    
    while((opt= getopt (argc, argv , "s:d:"))!=-1)
    {
        switch(opt)
        {
            case 's':
                segundos(atoi (optarg));
                break;

            case 'd':
                dias(atoi (optarg));
                break;

            case ':':
                printf("option needs a value\n");  
                break;

            case '?':
                printf("unknown option: %c\n", optopt); 
                break;
        }
    }
    
    return 0;
}