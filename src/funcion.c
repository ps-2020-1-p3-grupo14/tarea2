#include <stdio.h>
#include <unistd.h>  

void mostrar_mensaje(char*str, int valor ){
    printf("%s %d\n", str, valor);
};

void segundos(int s)
{   
    int minutos, horas;

    horas=s/3600;
    minutos=(s%3600)/60;
    s=(s%3600)%60;

    printf("horas\tminutos\tsegundos\n");
    printf("%d \t%d \t%d \n", horas, minutos, s);
};

void dias(int dias)
{
    int anos, meses;

    anos=dias/365;
    meses=(dias%365)/30;
    dias=(dias%365)%30;

    printf("años\tmeses\tdias\n");
    printf("%d \t %d \t %d\n", anos, meses, dias);
};